<?php
class Dispatcher{

    private $sysRoot;
    private $loginFlag;

    public function setSystemRoot($path){
	$this->sysRoot = rtrim($path, '/');//末尾のスラッシュ削除
    }

    public function dispatch(){

	// パラメーター取得
	$param = preg_replace('{'.dirname($_SERVER["SCRIPT_NAME"]).'/}', '', $_SERVER['REQUEST_URI']);
	//末尾の / を削除
	$param = preg_replace('{/?$}', '', $param);
	$params = array();
	if ('' != $param) {
	    // パラメーターを / で分割
	    $params = explode('/', $param);
	}

	// １番目のパラメーターをコントローラーとして取得
	$controller = "Index";
	if (0 < count($params)) {
	    $controller = '';
	    $tmp = explode('-', $params[0]);
	    foreach($tmp as $value){
		$controller .= ucfirst($value);
	    }
	}

	// １番目のパラメーターをもとにコントローラークラスインスタンス取得
        $controllerInstance = $this->getControllerInstance($controller);
        if (null == $controllerInstance) {
            $this->print404();
        }

	// 2番目のパラメーターをアクションとして取得
	$action= 'index';
	if (1 < count($params)) {
	    $action = '';
	    $tmp = explode('-', $params[1]);
	    foreach($tmp as $value){
		$action .= ucfirst($value);
	    }
	    $action[0] = strtolower($action[0]);
	}

	// アクションメソッドの存在確認
        if (false == method_exists($controllerInstance, $action . 'Action')) {
            $this->print404();
        }
	//コントローラ初期設定
	//$controllerInstance->setSystemRoot($this->sysRoot);
	$controllerInstance->setControllerAction($controller, $action);
	//処理実行
	$controllerInstance->run();
    }


    // コントローラークラスのインスタンスを取得
    private function getControllerInstance($controller){
        // クラス名＋"Controller"
        $className = $controller . 'Controller';
        // コントローラーファイル名
        $controllerFileName = sprintf('%s/controllers/%s.php', $this->sysRoot, $className);
        // ファイル存在チェック
        if (false == file_exists($controllerFileName)) {
            return null;
        }
        // クラスファイルを読込
        require_once $controllerFileName;
        // クラスが定義されているかチェック
        if (false == class_exists($className)) {
            return null;
        }
        // クラスインスタンス生成
        $controllerInstarnce = new $className();

        return $controllerInstarnce;
    }

    private function print404(){
	header("HTTP/1.0 404 Not Found");
	include('404.html');
	exit;
    }    
}
?>
