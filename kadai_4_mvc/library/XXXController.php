<?php
abstract class XXXController{
    protected $systemRoot;
    protected $controller = 'index';
    protected $action = 'index';
    protected $view;
    protected $templatePath;

    public function setSystemRoot($path)
    {
        $this->systemRoot = $path;
    }

    public function setControllerAction($controller, $action){ //コントローラとアクションの文字列設定
	$this->controller = $controller;
	$this->action = $action;
    }

    public function run(){
        try {
	    $this->initializeView(); // ビューの初期化
	    $this->preAction(); // 共通の前処理
	    $methodName = sprintf('%sAction', $this->action);
	    $this->$methodName(); // アクションメソッド
	    $this->view->display($this->templatePath);// 表示
        } catch (Exception $e) {
	    echo 'XXXControler->run() ERROR';
	}
    }

    protected function initializeView(){ //viewの初期化
	$this->view = new Smarty();
        $this->view->template_dir = sprintf('%s/views/templates/', ROOT_PATH);
        $this->view->compile_dir = sprintf('%s/views/templates_c/', ROOT_PATH);
	$this->templatePath = sprintf('%s/%s.php', $this->controller, $this->action);
    }
    
    protected function preAction(){ //前処理，オーバーライド前提
    }
}
