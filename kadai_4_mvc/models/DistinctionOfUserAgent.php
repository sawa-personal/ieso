<?php
class DistinctionOfUserAgent{
    private $ua;
    private $device;

    public function UserAgent(){
	$this->deviceCheck();
    }

    public function deviceCheck(){

	//ユーザーエージェント取得
	$this->ua = $_SERVER['HTTP_USER_AGENT'];

	if(strpos($this->ua,'iPhone') !== false){
	    //iPhone
	    $this->device = 'iphone';
	}
	elseif((strpos($this->ua,'Android') !== false) && (strpos($this->ua, 'Mobile') !== false)){
            //Android
            $this->device = 'android';
        }
	elseif((strpos($this->ua,'DoCoMo') !== false) or (strpos($this->ua,'KDDI') !== false) or (strpos($this->ua,'SoftBank') !== false) ){
	    $this->device = 'featurephone';
	}
        else{
            $this->device = 'pc';
        }
    }
    
    public function getDevice(){
        return $this->device;
    }
}
