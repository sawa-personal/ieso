<?php
//*** データ読み込み ***//
$file = 'kadai_2.txt'; 
$lin = file($file);
$res[1000]; //1000件まで，No,Name,Comment,Date,Password
for($i = 0; $i < count($line); $i++){
    $res[$i] = explode("<>", $line[$i]);
}
?>
<html>
    <body>
	<form action="" method="post">
	    <?php if($_POST['f_edit']){
		echo '<input type="hidden" name="f_edit" value="'.$_POST['del_num'].'">';
	    } ?>
	    <table>
		<tr><th>名前</th><td><input size="40" type="text" name="name"></td></tr>
		<tr><th>コメント</th><td><textarea cols="40" rows="8" name="comment"><?php if($_POST['f_edit'])
										     {
											 $tmp = $table[$_POST['edit']-1][2];
											 $tmp = str_replace("<br />","\r\n",$tmp);
											 $tmp = str_replace("&gt;",">",$tmp);
											 $tmp = str_replace("&lt;","<",$tmp);
											 echo $tmp;
										     } ?></textarea></td></tr>
		<tr><th><?php
			if($_POST['f_edit'] == "true"){
			    echo "パスワードを入力";
			} else {
			    echo "パスワードを設定";
			} ?></th><td><input size="8" type="password" name="password"></td></tr>
		<tr><td colspan="2"><input type="submit" value="<?php
								if($_POST['f_edit']){
								    echo "書き換える";
								} else {
								    echo "投稿";
								} ?>"></td></tr>
	    </table>
	</form>
	
	<form action="" method="post">
	    <input type="hidden" name="f_del" value="true">
	    <table>
		<tr>
		    <th>削除対象番号</th><td><input size="8" type="text" name="delnum"></td>
		    <th>パスワード</th><td><input size="8" type="password" name="del_password"></td>
		    <td colspan="2"><input type="submit" value="削除"></td></tr>
	    </table>
	</form>

	<form action="" method="post">
	    <input type="hidden" name="f_edit" value="true">
	    <table>
		<tr>
		    <th>編集対象番号</th><td><input size="8" type="text" name="editnum"></td>
		    <th>パスワード</th><td><input size="8" type="password" name="edit_password"></td>
		    <td colspan="2"><input type="submit" value="編集"></td>
		</tr>
	    </table>
	</form>


	<h2>削除フォーム</h2>
	<form action="" method="post">
	    <table>
		<?php
		if($_POST['f_del'] != "true"){
		    echo "<input type=\"hidden\" name=\"f_del\" value=\"true\">";
		    echo "<tr><th>削除対象番号</th><td><input cols=\"4\" type=\"text\" name=\"del\"></td></tr>";
		}
		if($_POST['f_del'] == "true"){
		    echo "<input type=\"hidden\" name=\"delmode\" value=\"".$_POST['del']."\">";
		    echo "<tr><th>パスワードを入力</th><td><input cols=\"4\" type=\"text\" name=\"pw_del\"></td></tr>\n";
		} ?>
		<tr><td colspan="2"><input type="submit" value="削除" /></td></tr>
	    </table>
	</form>

	<?php
	//*** 書き込み ***//
	if($_POST['f_comment'] == "true"){
	    $time = date('Y/m/d-H:i:s');
	    $comment = str_replace("<","&lt;",$_POST['comment']);
	    $comment = str_replace(">","&gt;",$comment);
	    $comment = str_replace("\r\n","<br />",$comment);
	    if(($_POST['editmode'] != "") && ($table[(int)$_POST['editmode']-1][4] == $_POST['password'])){
		$table[(int)$_POST['editmode']-1][2] = $comment;
		$table[(int)$_POST['editmode']-1][3] = $time;
	    } else {
		$table[$number][0] = $number+1;
		if($_POST['name'] == ""){
		    $table[$number][1] = "Anonymous";
		} else {
		    $table[$number][1] = $_POST['name'];
		}
		$table[$number][2] = $comment;
		$table[$number][3] = $time;
		$table[$number][4] = $_POST['password'];
	    }
	}
	?>

	<?php
	//*** 削除 ***//
	if(($table[(int)$_POST['delmode']-1][4] == $_POST['pw_del'])){
	    $table[(int)$_POST['delmode']-1][2] = "この投稿は削除されました．";
	}
	?>

	<table>
	    <tr><th>No</th><th>投稿者</th><th>コメント</th><th>最終編集日</th></tr>
	    <?php
	    //*** 掲示板の表示 ***//
	    if(($_POST['f_comment'] == "true") && ($_POST['editmode'] == "")){
		$number++;
	    }
	    $buf = file_get_contents($file);
	    $ary = explode("\n", $buf);
	    for($i = 0; $i < $number; $i++){
		echo "<tr><td>{$table[$i][0]}</td><td>{$table[$i][1]}</td><td>{$table[$i][2]}</td><td>{$table[$i][3]}</td></tr>\n";
	    }
	    ?>

	    <?php
	    //*** データ書き出し ***//
	    for($i = 0; $i < $number; $i++){
		$ary[$i] = implode("<>", $table[$i]);
	    }
	    $buf = implode("\n", $ary);
	    file_put_contents($file, $buf);
	    ?>
	</table>	    
    </body>
</html>
