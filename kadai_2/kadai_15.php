<?php 
$pdo = new PDO('mysql:host=localhost;dbname=co_647_it_3919_com;charset=utf8', 'co-647.it.3919.c', 'jBi8Hbd');
?>
<html>
    <body>
	<form action="" method="post">
	    <?php
	    /*エラー警告*/
	    //投稿エラー
	    if(($_POST['f_comment'] == "true" or $_POST['f_edited']) and $_POST['comment'] == ""){
		echo "<span style=\"color:#c00;\">Submit Error:コメントが未入力です．</span><br />";
		$_POST['f_comment'] = "";
		$_POST['f_edited'] = "";
	    }
	    //編集エラー
	    if($_POST['f_edit'] == "true"){
		$stmt = $pdo->prepare('SELECT * FROM bbs WHERE no = :edit_no');
		$stmt->bindValue(':edit_no', $_POST[editnum], PDO::PARAM_INT);
		$stmt->execute();
		$edit = $stmt->fetch(PDO::FETCH_ASSOC);
		if(!$edit){
		    echo "<span style=\"color:#c00;\">Edit Error:投稿が存在しません．</span><br />";
		    $_POST['f_edit'] = "";
		} else if($edit['password'] != $_POST['edit_password']){
		    echo "<span style=\"color:#c00;\">Edit Error:パスワードが間違っています．</span><br />";
		    $_POST['f_edit'] = "";
		}
	    }
	    //削除エラー
	    if($_POST['f_del'] == "true"){
		$stmt = $pdo->prepare('SELECT password FROM bbs WHERE no = :del_no');
		$stmt->bindValue(':del_no', $_POST[delnum], PDO::PARAM_INT);
		$stmt->execute();
		$re = $stmt->fetch(PDO::FETCH_ASSOC);
		if(!$re){
		    echo "<span style=\"color:#c00;\">Delete Error:投稿が存在しません．</span><br />";
		} else if($re['password'] != $_POST[del_password]){
		    echo "<span style=\"color:#c00;\">Delete Error:パスワードが間違っています．</span><br />";
		    $_POST['f_del'] = "";
		}
	    }
	    ?>
	    <input type="hidden"
		   name="<?php
			 if($_POST['f_edit'] == "true"){
			     echo f_edited;
			 } else {
			     echo f_comment;
			 }?>"
		   value="<?php
			  if($_POST['f_edit'] == "true"){
			      echo $edit['no'];
			  } else {
			      echo "true";
			  }?>">
	    <table>
		<tr><th>名前</th><td><input size="40" type="text" name="name" value="<?php
										     if($_POST['f_edit'] == "true"){
											 echo $edit['name'];
										     } ?>"></td></tr>
		<tr><th>コメント</th><td><textarea cols="40" rows="8" name="comment"><?php
										     if($_POST['f_edit'] == "true"){
											 echo $edit['comment'];
										     } ?></textarea></td></tr>
		<?php if($_POST['f_edit'] == "true"){echo "<!--";}?>
		<tr><th>パスワード</th><td><input size="8" type="password" name="password"></td></tr>
		<?php if($_POST['f_edit'] == "true"){echo "-->";}?>
		<tr><td colspan="2"><input type="submit" value="<?php
								if($_POST['f_edit'] == "true"){
								    echo 編集;
								} else {
								    echo 投稿;
								}?>"></td></tr>
	    </table>
	</form>

	<form action="" method="post">
	    <input type="hidden" name="f_del" value="true">
	    <table>
		<tr>
		    <th>削除対象番号</th><td><input size="8" type="text" name="delnum"></td>
		    <th>パスワード</th><td><input size="8" type="password" name="del_password"></td>
		    <td colspan="2"><input type="submit" value="削除"></td></tr>
	    </table>
	</form>

	<form action="" method="post">
	    <input type="hidden" name="f_edit" value="true">
	    <table>
		<tr>
		    <th>編集対象番号</th><td><input size="8" type="text" name="editnum"></td>
		    <th>パスワード</th><td><input size="8" type="password" name="edit_password"></td>
		    <td colspan="2"><input type="submit" value="編集"></td>
		</tr>
	    </table>
	</form>

	<?php
	//*** 書き込み ***//
	if($_POST['f_comment'] == "true"){
	    $stmt = $pdo -> prepare("INSERT INTO bbs (no,name,comment,password) VALUES ('', :name, :comment, :password)");
	    $stmt -> bindValue(':name', $_POST['name'] == "" ? "Anonymous" : $_POST['name'], PDO::PARAM_STR);
	    $stmt -> bindValue(':comment', $_POST['comment'], PDO::PARAM_STR);
	    $stmt -> bindValue(':password', $_POST['password'], PDO::PARAM_STR);
	    $stmt -> execute();
	}
	?>

	<?php
	//*** 削除 ***//
	if($_POST['f_del'] == "true"){
	    $stmt = $pdo->prepare('DELETE FROM bbs WHERE no = :del_no');
	    $stmt->bindValue(':del_no', $_POST[delnum], PDO::PARAM_INT);
	    $stmt->execute();
	}
	?>

	<?php
	//*** 編集 ***//
	if($_POST['f_edited'] != ""){
	    $stmt = $pdo->prepare('UPDATE bbs SET name = :name, comment = :comment WHERE no = :edit_num');
	    $stmt->bindValue(':name', $_POST['name'], PDO::PARAM_STR);
	    $stmt->bindValue(':comment', $_POST['comment'], PDO::PARAM_STR);
	    $stmt->bindValue(':edit_num', $_POST['f_edited'], PDO::PARAM_INT);
	    $stmt->execute();
	}
	?>

	<table>
	    <tr><th>No</th><th>投稿者</th><th>コメント</th><th>投稿日</th></tr>
	    <?php
	    //*** 掲示板の表示 ***//
	    $stmt = $pdo->query('SELECT * FROM bbs ORDER BY no ASC');
	    while($re = $stmt->fetch(PDO::FETCH_ASSOC)){
		echo "<tr><td>{$re['no']}</td><td>{$re['name']}</td><td>{$re['comment']}</td><td>{$re['time']}</td></tr>\n";
	    }
	    ?>
	</table>
    </body>
</html>
