<html>
    <body>
	<?php $action = "./kadai_4.php"; ?>

	<h2>投稿フォーム</h2>
	<?php 
	$name = $_POST['name'];
	$comment = $_POST['comment'];
	$f_comment = $_POST['f_comment'];
	?>
	<form action="<?php echo $action; ?>" method="post">
	    <input type="hidden" name="f_comment" value="true">
	    <table>
		<tr><th>名前</th><td><input type="text" name="name"></td></tr>
		<tr><th>コメント</th><td><input type="text" name="comment"></td></tr>
		<tr><td colspan="2"><input type="submit" value="投稿" /></td></tr>
	    </table>
	</form>

	<h2>削除フォーム</h2>
	<?php
	$del = $_POST['del'];
	$f_del = $_POST['f_del'];
	?>
	<form action="<?php echo $action; ?>" method="post">
	    <input type="hidden" name="f_del" value="true">
	    <table>
		<tr><th>削除対象番号</th><td><input type="text" name="del"></td></tr>
		<tr><td colspan="2"><input type="submit" value="削除" /></td></tr>
	    </table>
	</form>

	<?php $file = 'kadai_2.txt'; ?>
	
	<!--書込-->
	<?php
	$fp = fopen($file, 'r');
	for($number = 0; fgets($fp); $number++);
	fclose($fp);

	$buf = file_get_contents($file);
	if($number > 0){
	    $buf .= "\n";
	}
	$buf .= ++$number;
	$buf .= "<>";
	$buf .= $name;
	$buf .= "<>";
	$buf .= $comment;
	$buf .= "<>";
	$buf .= date('Y年m月d日H時i分s秒');
	if($f_comment == "true"){
	    file_put_contents($file, $buf);
	}
	?>

	<!--書き込みデータの2次元配列化-->
	<?php
	$buf = file_get_contents($file);

	$ary = explode("\n", $buf);
	$table[1000]; //No,Name,Comment,Date
	for($i = 0; $i < $number; $i++){
	    $table[$i] = explode("<>", $ary[$i]);
	}
	?>

	<!--削除-->
	<?php
	if($f_del == "true"){
	    for($i = 0; $i < $number; $i++){
		if($i + 1 == $del){
		    $table[$i][2] = "この投稿は削除されました．";
		    break;
		}
	    }

	    for($i = 0; $i < $number; $i++){
		$ary[$i] = implode("<>", $table[$i]);
	    }

	    $buf = implode("\n", $ary);
	    file_put_contents($file, $buf);
	}
	?>

	<!--表示-->
	<table>
	    <?php
	    $buf = file_get_contents($file);
	    $ary = explode("\n", $buf);
	    
	    for($i = 0; $i < $number; $i++){
		echo "<tr>";
		echo "<td>{$table[$i][0]}</td>";
		echo "<td>{$table[$i][1]}</td>";
		echo "<td>{$table[$i][2]}</td>";
		echo "<td>{$table[$i][3]}</td>";
		echo "<tr>\n";
	    }
	    ?>
	</table>	    
    </body>
</html>
