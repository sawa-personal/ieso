<html>
    <body>
	<h2>投稿フォーム</h2>

	<form action="" method="post">
	    <table>
		<tr><th>名前</th><td><input type="text" name="name"></td></tr>
		<tr><th>コメント</th><td><input type="text" name="comment"></td></tr>
		<tr><td colspan="2"><input type="submit" value="投稿" /></td></tr>
	    </table>
	</form>

	<h2>削除フォーム</h2>
	<form action="" method="post">
	    <input type="hidden" name="f_del" value="true">
	    <table>
		<tr><th>削除対象番号</th><td><input type="text" name="delnum"></td></tr>
		<tr><td colspan="2"><input type="submit" value="削除" /></td></tr>
	    </table>
	</form>

	<?php
	$txt = 'data_sec.txt';
	//*****書込みを2次元配列に展開
	$line = file($txt);

	for($i = 0; $i < count($line); $i++){
	    $res[$i] = explode('<>', $line[$i]);
	}
	
	//*****書込
	if($_POST['comment'] != ''){
	    $buf = file_get_contents($txt); //txtの中身を文字列として取得．

	    //レス番号を決める
	    if($buf == ''){
		$num = 1;
	    } else {
		$latest = explode('<>', end($line)); //最後の書き込みを取得し，1次元配列化
		$num = $latest[0] + 1;
	    }

	    $buf .= $num.'<>'.$_POST['name'].'<>'.$_POST['comment'].'<>'.date('Y-m-d H:i:s')."\n";
	    file_put_contents($txt, $buf);
	}

	//*****削除
	if($_POST['f_del'] == 'true'){
	    for($i = 0; $i < count($line); $i++){
		if($res[$i][0] == $_POST['delnum']){
		    unset($line[$i]);
		    break;
		}
	    }
	    file_put_contents($txt, implode($line));
	}
	?>

	<table>
	    <?php
	    //表示処理
	    //***** 再び書込みを2次元配列に展開
	    $line = file($txt);

	    for($i = 0; $i < count($line); $i++){
		$res[$i] = explode('<>', $line[$i]);
	    ?>
		<tr><td><?=$res[$i][0]?></td><td><?=$res[$i][1]?></td><td><?=$res[$i][2]?></td><td><?=$res[$i][3]?></td></tr>
	    <?php } ?>
	</table>
    </body>
</html>
