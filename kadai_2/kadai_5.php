<html>
    <body>
	<?php
	//*** preproc ***//
	$file = 'kadai_2.txt'; 
	
	//書き込みデータの2次元配列化
	$buf = file_get_contents($file);
	$ary = explode("\n", $buf);
	$number = count($ary); //投稿数
	$table[1000]; //No,Name,Comment,Date
	for($i = 0; $i < $number; $i++){
	    $table[$i] = explode("<>", $ary[$i]);
	}
	if($buf == ""){
	    $number--;
	}
	?>

	<h2>編集フォーム</h2>
	<form action="" method="post">
	    <input type="hidden" name="f_edit" value="true">
	    <table>
		<tr><th>編集対象番号</th><td><input size="4" type="text" name="edit"></td></tr>
		<tr><td colspan="2"><input type="submit" value="編集" /></td></tr>
	    </table>
	</form>

	<h2>投稿フォーム</h2>
	<form action="" method="post">
	    <input type="hidden" name="f_comment" value="true">
	    <?php if($_POST['f_edit'] == "true"){
		echo "<input type=\"hidden\" name=\"editmode\" value=\"".$_POST['edit']."\">";
	    } ?>
	    <table>
		<tr><th>名前</th><td><input size="40" type="text" name="name"></td></tr>
		<tr><th>コメント</th><td><textarea cols="40" rows="8" name="comment"><?php if($_POST['f_edit'] == "true")
										     {
											 $tmp = $table[$_POST['edit']-1][2];
											 $tmp = str_replace("<br />","\r\n",$tmp);
											 $tmp = str_replace("&gt;",">",$tmp);
											 $tmp = str_replace("&lt;","<",$tmp);
											 echo $tmp;
										     } ?></textarea></td></tr>
		<tr><td colspan="2"><input type="submit" value="<?php
								if($_POST['f_edit'] == "true"){
								    echo "書き換える";
								} else {
								    echo "投稿";}
								?>"></td></tr>
	    </table>
	</form>
	
	<h2>削除フォーム</h2>
	<form action="" method="post">
	    <input type="hidden" name="f_del" value="true">
	    <table>
		<tr><th>削除対象番号</th><td><input cols="4" type="text" name="del"></td></tr>
		<tr><td colspan="2"><input type="submit" value="削除" /></td></tr>
	    </table>
	</form>

	<?php
	//*** 書き込み ***//
	if($_POST['f_comment'] == "true"){
	    $time = date('Y/m/d-H:i:s');
	    $comment = str_replace("<","&lt;",$_POST['comment']);
	    $comment = str_replace(">","&gt;",$comment);
	    $comment = str_replace("\r\n","<br />",$comment);
	    if($_POST['editmode'] != ""){
		$table[(int)$_POST['editmode']-1][2] = $comment;
		$table[(int)$_POST['editmode']-1][3] = $time;
	    } else {
		$table[$number][0] = $number+1;
		if($_POST['name'] == ""){
		    $table[$number][1] = "Anonymous";
		} else {
		    $table[$number][1] = $_POST['name'];
		}
		$table[$number][2] = $comment;
		$table[$number][3] = $time;
	    }
	}
	?>

	<?php
	//*** 削除 ***//
	if($_POST['f_del'] == "true"){
	    for($i = 0; $i < $number; $i++){
		if($i == $_POST['del']){
		    $table[$i][2] = "この投稿は削除されました．";
		    break;
		}
	    }
	}
	?>

	<table>
	    <tr><th>No</th><th>投稿者</th><th>コメント</th><th>最終編集日</th></tr>
	    <?php
	    //掲示板の表示
	    if(($_POST['f_comment'] == "true") && ($_POST['editmode'] == "")){
		$number++;
	    }
	    $buf = file_get_contents($file);
	    $ary = explode("\n", $buf);
	    for($i = 0; $i < $number; $i++){
		echo "<tr><td>{$table[$i][0]}</td><td>{$table[$i][1]}</td><td>{$table[$i][2]}</td><td>{$table[$i][3]}</td></tr>\n";
	    }
	    ?>

	    <?php
	    //上書き
	    for($i = 0; $i < $number; $i++){
		$ary[$i] = implode("<>", $table[$i]);
	    }

	    $buf = implode("\n", $ary);
	    file_put_contents($file, $buf);

	    ?>
	</table>	    
    </body>
</html>
