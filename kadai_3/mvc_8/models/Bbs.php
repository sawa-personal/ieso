<?php

class Bbs{

    private $pdo;

    public function __construct(){
        $this->pdo = new PDO('mysql:host=localhost;dbname=co_647_it_3919_com;charset=utf8', 'co-647.it.3919.c', 'jBi8Hbd');
    }


    public function create_table(){
	//$this->pdo -> exec('DROP TABLE IF EXISTS bbs2');
	$this->pdo->query( "CREATE TABLE IF NOT EXISTS bbs2"
			  ."("
			  ."no INT auto_increment primary key,"
			  ."id CHAR(8),"
			  ."name CHAR(255),"
			  ."comment VARCHAR(2048),"
			  ."object LONGBLOB," /*バイナリ*/
			  ."ext char(8)," /*拡張子*/
			  ."time TIMESTAMP DEFAULT CURRENT_TIMESTAMP"
			  .");");
    }

    /*スレッド一覧を取得*/
    public function getThreadList(){
	//最新の10件を取得
    }

    /*スレッドの内容を取得*/
    public function getThread(){
	$stmt = $this -> pdo -> query("SELECT * FROM bbs2");
	return $stmt -> fetchAll();
    }

    //スレッド作成public function makeThread(){

    //スレッド削除  public function deleteThread(){


    public function getExt($path){
	return pathinfo($path, PATHINFO_EXTENSION);
    }

    /*書込み*/
    public function writeRes($id, $name, $comment, $upfile, $upfiletmp){
	$object = file_get_contents($upfiletmp);
	$ext = $this -> getExt($upfile);
	echo $ext;
	$stmt = $this -> pdo -> prepare("INSERT INTO bbs2 (no,id,name,object,ext,comment) VALUES ('', :id, :name, :object, :ext, :comment)");
	$stmt -> bindValue(':id', $id, PDO::PARAM_STR);
	$stmt -> bindValue(':name', $name == "" ? "Anonymous" : $name, PDO::PARAM_STR);
	$stmt -> bindValue(':comment', $comment, PDO::PARAM_STR);
	$stmt -> bindValue(':object', $object, PDO::PARAM_STR);
	$stmt -> bindValue(':ext', $ext, PDO::PARAM_STR);
	$stmt -> execute();
    }	

    /*書込み削除*/
    public function deleteRes(){

    }

    /*書込み編集*/
    public function editRes(){

    }

    public function showImage($id, $ext){
	/*
	   $stmt = $this->pdo->query("SELECT * FROM bbs2 WHERE no='{$id}'");
	   $re = $this->getThread();
	 */
	
	switch ($ext) {
	    case 'png': $mime = 'image/png'; break;
	    case 'jpg': $mime = 'image/jpeg'; break;
	    case 'gif': $mime = 'image/gif'; break;
	    case 'mp4': $mime = 'video/mp4'; break;
	    case 'ogv': $mime = 'video/ogg'; break;
	}

	header("Content-type: $mime");
	$re = $this->getThread();
	echo $re[$id-1]['object'];
    }

}
