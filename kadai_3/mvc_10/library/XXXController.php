<?php

abstract class XXXController{

    protected $view;
    protected $templatePath;
    protected $controller = 'index';
    protected $action = 'index';

    protected function initializeView(){ //viewの初期化
	$this -> view = new TemplateEngine();
        //$this->view->template_dir = sprintf('%s/view/templates/', $this->systemRoot);
        //$this->view->compile_dir = sprintf('%s/view/templates_c/', $this->systemRoot);
        $this->templatePath = sprintf('%s/%s.php', $this->controller, $this->action);
    }
    
    public function setControllerAction($controller, $action){ //コントローラとアクションの文字列設定
	$this->controller = $controller;
	$this->action = $action;
    }

    public function run(){
        try {
            $this->initializeView(); // ビューの初期化
            $this->preAction(); // 共通の前処理
            $methodName = sprintf('%sAction', $this->action);
            $this->$methodName(); // アクションメソッド  
            $this->view->show($this->templatePath);// 表示
        } catch (Exception $e) {
        }
    }

    protected function preAction(){ //前処理，オーバーライド前提
    }

}
