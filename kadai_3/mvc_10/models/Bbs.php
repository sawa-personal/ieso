<?php
class Bbs{

    private $pdo;

    public function __construct(){
        $this->pdo = new PDO('mysql:host=localhost;dbname=co_647_it_3919_com;charset=utf8', 'co-647.it.3919.c', 'jBi8Hbd');
    }

    public function createtable(){
	//$this->pdo -> exec("DROP TABLE IF EXISTS threads");
	$this->pdo->query( "CREATE TABLE IF NOT EXISTS threads"
			  ."("
			  ."id INT auto_increment PRIMARY KEY," //掲示板ID
			  ."title CHAR(255)," //掲示板タイトル
			  ."count INT," //レス数
			  ."delflag BOOL" //削除フラグ
			  .");");
    }

    public function createThread($uid, $name, $title, $comment, $upfile){
	/*
	   $this->pdo -> exec("DROP TABLE IF EXISTS th1");
	   $this->pdo -> exec("DROP TABLE IF EXISTS th2");
	   $this->pdo -> exec("DROP TABLE IF EXISTS th3");
	   $this->pdo -> exec("DROP TABLE IF EXISTS th4");
	   $this->pdo -> exec("DROP TABLE IF EXISTS th5");
	   $this->pdo -> exec("DROP TABLE IF EXISTS th6");
	   $this->pdo -> exec("DROP TABLE IF EXISTS th7");
	   $this->pdo -> exec("DROP TABLE IF EXISTS th8");
	   $this->pdo -> exec("DROP TABLE IF EXISTS th9");	
	 */
	//スレ一覧に追加
	$stmt = $this -> pdo -> prepare("INSERT INTO threads (id, title, count, delflag) "
				       ."VALUES ('', :title, :count, :delflag)");
	$stmt -> bindValue(':title', $title, PDO::PARAM_STR);
	$stmt -> bindValue(':delflag', false, PDO::PARAM_BOOL);
	$stmt -> bindValue(':count', 1, PDO::PARAM_INT);
	$stmt -> execute();

	//スレ一覧から最大のID(=ここで作成したスレのID)を取得
	$stmt = $this->pdo->query("SELECT id FROM threads WHERE id = (SELECT MAX(id) FROM threads)");
	$re = $stmt->fetch(PDO::FETCH_ASSOC);
	$id = $re['id'];

	//スレ作成
	$object = file_get_contents($upfile['tmp_name']);
	$ext = $this -> getExt($upfile['name']);
	$this->pdo->query( "CREATE TABLE IF NOT EXISTS th{$id}"
			  ."("
			  ."no INT auto_increment primary key," //投稿番号
			  ."uid CHAR(8)," //投稿者id
			  ."name CHAR(255),"
			  ."comment VARCHAR(2048),"
			  ."object LONGBLOB," //
			  ."ext char(8)," //拡張子
			  ."time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,"
			  ."delflag BOOL" //削除フラグ
			  .");");

	//スレに追加
	$stmt = $this->pdo->prepare("INSERT INTO th{$id} (no, uid, name, comment, object, ext, delflag) "
				   ."VALUES ('', :uid, :name, :comment, :object, :ext, :delflag)");
	$stmt -> bindValue(':uid', $uid, PDO::PARAM_STR);
	$stmt -> bindValue(':name', $name, PDO::PARAM_STR);
	$stmt -> bindValue(':comment', $comment, PDO::PARAM_STR);
	$stmt -> bindValue(':object', $object, PDO::PARAM_STR);
	$stmt -> bindValue(':ext', $ext, PDO::PARAM_STR);
	$stmt -> bindValue(':delflag', false, PDO::PARAM_STR);
	$stmt -> execute();
    }


    //スレッド一覧を取得
    public function getThreads(){
	$stmt = $this->pdo->query("SELECT * FROM threads WHERE delflag = false ORDER BY id DESC;");
	$threads = $stmt->fetchAll(); //全スレッドを取得

	$i = 0;
	foreach($threads as $line){
	    $stmt = $this->pdo->query("SELECT * FROM th{$line['id']}");
	    $th = $stmt->fetch(PDO::FETCH_ASSOC);
	    $re[$i]['id'] = $line['id'];
	    $re[$i]['title'] = $line['title'];
	    $re[$i]['count'] = $line['count'];
	    $re[$i]['name'] = $th['name'];
	    $re[$i]['comment'] = $th['comment'];
	    $i++;
	}
	return $re;
    }

    //スレタイを取得
    public function getThreadTitle($id){
	$stmt = $this->pdo->query("SELECT title FROM threads WHERE id = {$id}");
	$re = $stmt->fetch(PDO::FETCH_ASSOC);
	return $re['title'];
    }

    //書込み数を取得
    public function getNumberOfRes($id){
	$stmt = $this->pdo->query("SELECT * FROM th{$id} WHERE delflag = false");
	return $stmt->rowCount();
    }

    //スレッド削除
    public function deleteThread($id){
	$stmt = $this -> pdo -> prepare("UPDATE threads SET delflag =:delflag WHERE id = '{$id}'");
	$stmt->bindValue(':delflag', true, PDO::PARAM_STR);
	$stmt->execute();	
    }

    //スレッドの内容を取得
    public function getThread($id){
	$stmt = $this -> pdo -> query("SELECT * FROM th{$id}");
	return $stmt -> fetchAll();
    }

    //拡張子を取得
    public function getExt($path){
	return mb_strtolower(pathinfo($path, PATHINFO_EXTENSION));
    }

    /*書込み*/
    public function writeRes($id, $uid, $name, $comment, $upfile){
	$this -> pdo -> query("UPDATE threads SET count = count + 1 WHERE id = '{$id}'");

	$object = file_get_contents($upfile['tmp_name']);
	$ext = $this -> getExt($upfile['name']);
	$stmt = $this -> pdo -> prepare("INSERT INTO th{$id} (no,uid,name,object,ext,comment,delflag) "
				       ."VALUES ('', :uid, :name, :object, :ext, :comment, :delflag)");
	$stmt -> bindValue(':uid', $uid, PDO::PARAM_STR);
	$stmt -> bindValue(':name', $name == "" ? "Anonymous" : $name, PDO::PARAM_STR);
	$stmt -> bindValue(':comment', $comment, PDO::PARAM_STR);
	$stmt -> bindValue(':object', $object, PDO::PARAM_STR);
	$stmt -> bindValue(':ext', $ext, PDO::PARAM_STR);
	$stmt -> bindValue(':delflag', false, PDO::PARAM_STR);
	$stmt -> execute();
    }	

    /*書込み削除*/
    public function deleteRes($id, $no){
	$stmt = $this -> pdo -> prepare("UPDATE th{$id} SET delflag =:delflag WHERE no = '{$no}'");
	$stmt->bindValue(':delflag', true, PDO::PARAM_STR);
	$stmt->execute();
    }

    /*書込み編集*/
    public function editRes($id, $no, $comment){
	$stmt = $this -> pdo -> prepare("UPDATE th{$id} SET comment =:comment WHERE no = '{$no}'");
	$stmt->bindValue(':comment', $comment, PDO::PARAM_STR);
	$stmt->execute();
    }

    //画像を表示
    public function showImage($id, $no, $ext){
	switch ($ext) {
	    case 'jpg': $mime = 'image/jpeg'; break;
	    case 'gif': $mime = 'image/gif'; break;
	    case 'png': $mime = 'image/png'; break;
	    case 'mp4': $mime = 'video/mp4'; break;
	}

	header("Content-type: $mime");
	$re = $this->getThread($id);
	echo $re[$no-1]['object'];
    }
}
