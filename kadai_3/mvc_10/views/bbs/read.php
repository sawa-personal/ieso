<?php include 'header.php'; ?>

<?php $page = $_GET['page'] == '' ? 1 : $_GET['page']; ?>

<!-- スレッド投稿フォーム -->
<?php if($doesLogin) { ?>
    <form action="<?=ROOT_URL?>/bbs/create_done" method="post" enctype="multipart/form-data">
	<input type="hidden" name="name" value="<?=$userName?>">
	<table>
	    <tr><th>スレッドタイトル</th><td><input type="text" name="title"></td></tr>
	    <tr><th>コメント</th><td><textarea cols="40" rows="8" name="comment"></textarea></td></tr>
	    <tr><th>画像/動画</th><td><input type="file" name="upfile"></td></tr>
	</table>
	<div style="text-align:center;"><input type="submit" value="スレッド作成"/></div>
    </form>
<?php } ?>

<h2>スレッド一覧</h2>

<ul>
    <?php for($i = 5 * ($page - 1); $i < 5 * $page && $i < count($threads); $i++) { ?>
	<li style="margin:0.5em 0;"><a href="<?=ROOT_URL?>/bbs/reply/?id=<?=$threads[$i]['id']?>"><?=$threads[$i]['title']?> (<?=$threads[$i]['count']?>件)</a></li>
    <?php } ?>
</ul>

<div style="text-align:center">

    <?php if($page - 1 <= 0) { ?>
	＜＜新しい5件
    <?php } else { ?>
	<a href="<?=ROOT_URL?>/bbs/read/?page=<?=(int)($page-1)?>">＜＜新しい5件</a>
    <?php } ?>

    <?php if(5 * $page > count($threads) - 1) { ?>
	古い5件＞＞
    <?php } else { ?>
	<a href="<?=ROOT_URL?>/bbs/read/?page=<?=(int)($page+1)?>">古い5件＞＞</a>
<?php } ?>
</div>
<?php include 'footer.php'; ?>
