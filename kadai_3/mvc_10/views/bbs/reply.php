<?php include 'header.php'; ?>

<!-- 表示処理 -->
<dl>
    <?php
    foreach($thread as $res){ 
	if($res['delflag'] == '0'){ 
    ?>
	<dt><?=$res['no']?> : <b><?=$res['name']?></b> <?=$res['time']?>
	    <?php if($res['uid'] == $userID) { ?>
		<form action="<?=ROOT_URL?>/bbs/edit_conf" method="post" style="display: inline;">
	    <input type="hidden" name="id" value="<?=$_GET['id']?>">
	    <input type="hidden" name="no" value="<?=$res['no']?>">
	    <input type="hidden" name="comment" value="<?=$res['comment']?>">
	    <input type="submit" value="編集" />
		</form>
		<form action="<?php
			      if($res['no'] == 1){
				  echo ROOT_URL.'/bbs/destroy_conf';
			      } else {
				  echo ROOT_URL.'/bbs/delete_conf';
			      }?>" method="post" style="display: inline;">
		    <input type="hidden" name="id" value="<?=$_GET['id']?>">
		    <input type="hidden" name="no" value="<?=$res['no']?>">
		    <input type="hidden" name="comment" value="<?=$res['comment']?>">
		    <input type="submit" value="削除" />
		</form>
	    <?php } ?>
	</dt>
	<dd>
	    <?php if($res['ext'] == 'png' || $res['ext'] == 'jpg' || $res['ext'] == 'gif'){?>
		<img src="<?=ROOT_URL?>/bbs/image/?id=<?=$_GET['id']?>&no=<?=$res['no']?>&type=<?=$res['ext']?>" />
	    <?php } ?>
	    <?php if($res['ext'] == 'mp4'){?>
		<video controls>
		    <source src="<?=ROOT_URL?>/bbs/image/?id=<?=$_GET['id']?>&no=<?=$res['no']?>&type=<?=$res['ext']?>">
		</video>
	    <?php } ?>
	    <p><?=$res['comment']?></p>
	</dd>
    <?php
    }
    }
    ?>
</dl>

<!-- 投稿フォーム -->
<?php if($doesLogin) { ?>
    <form action="<?=ROOT_URL?>/bbs/post_done" method="post" enctype="multipart/form-data">
	<input type="hidden" name="id" value="<?=$_GET['id']?>">
	<input type="hidden" name="name" value="<?=$userName?>">
	<table>
	    <tr><th>コメント</th><td><textarea cols="40" rows="8" name="comment"></textarea></td></tr>
	    <tr><th>画像/動画</th><td><input type="file" name="upfile"></td></tr>
	</table>
	<div style="text-align:center;"><input type="submit" value="投稿" /></div>
</form>
<?php } ?>

<?php include 'footer.php'; ?>
