<?php
class BbsController extends XXXController{

    function preAction(){ //全ページ共通処理
	session_start();
	$doesLogin = isset($_SESSION['username']);

	$param = ereg_replace(dirname($_SERVER["SCRIPT_NAME"]).'/', '', $_SERVER['REQUEST_URI']); // パラメーター取得
	$param = ereg_replace('/?$', '', $param);
	$params = array();
	if ('' != $param) {
	    $params = explode('/', $param);
	}
	$currentPath = ROOT_URL.'/'.$params[0].'/'.$params[1];
	$currentPath = rtrim($currentPath, '/'); //末尾のスラッシュ削除
	if(!$doesLogin && $currentPath != ROOT_URL && $currentPath != ROOT_URL.'/bbs/read' && $currentPath != ROOT_URL.'/bbs/image' && $currentPath != ROOT_URL.'/bbs/reply' ){
	    header('Location: '.ROOT_URL);
	    exit;
	}

	$currentURL = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

	$this -> view -> currentURL = $currentURL;
	$this -> view -> doesLogin = $doesLogin;
	$this -> view -> userName = $doesLogin ? $_SESSION['username'] : 'Guest';
	$this -> view -> userID = $_SESSION['id'];
    }

    //スレッド一覧
    public function readAction(){
	$bbs = new Bbs();
	$this -> view -> pagetitle = '掲示板トップ';
	$this->view->threads = $bbs->getThreads();
    }

    //スレッド表示
    public function replyAction(){
	$bbs = new Bbs();
	$this -> view -> pagetitle = $bbs -> getThreadTitle($_GET['id']);
	$this -> view -> thread = $bbs -> getThread($_GET['id']);
    }

    //スレ立て
    public function create_doneAction(){
	$bbs = new Bbs();
	$bbs -> createtable();
	$bbs -> createThread($_SESSION['id'], $_SESSION['username'], $_POST['title'], $_POST['comment'], $_FILES['upfile']);
	$this -> view -> pagetitle = 'スレッド作成完了';
    }
    //スレ削除
    public function destroy_confAction(){
	$this -> view -> pagetitle = 'スレッド削除確認';
    }

    public function destroy_doneAction(){
	$bbs = new Bbs();
	$bbs->deleteThread($_POST['id']);
	$this -> view -> pagetitle = 'スレッド削除完了';
    }

    //書込み
    public function post_doneAction(){
	$bbs = new Bbs();
	$bbs -> writeRes($_POST['id'], $_SESSION['id'],$_POST['name'],$_POST['comment'], $_FILES['upfile']);
	$this -> view -> pagetitle = '書込み完了';
    }

    //画像表示
    public function imageAction(){
	$bbs = new Bbs();
	$bbs -> showImage($_GET['id'], $_GET['no'], $_GET['type']);
    }

    //編集
    public function edit_confAction(){}

    public function edit_doneAction(){
	$bbs = new Bbs();
	$bbs -> editRes($_POST['id'], $_POST['no'], $_POST['comment']);
    }

    //書込み削除
    public function delete_confAction(){}

    public function delete_doneAction(){
	$bbs = new Bbs();
	$bbs -> deleteRes($_POST['id'], $_POST['no']);
    }
}
