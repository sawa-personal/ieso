<?php
class SignController{
    public function upAction(){
	$view = new TemplateEngine();
	$view -> template_dir = '/views/';
	$view -> pagetitle = "会員登録";
	$view -> action = 'http://co-647.99sv-coco.com/kadai_3/kadai_7/sign/up_conf';
	$view -> show('up.php');
    }

    public function up_confAction(){
	$view = new TemplateEngine();
	if($_POST['password'] == ''){
	    echo 'パスワードを入力してください．';
	    exit();
	}
	$view -> template_dir = '/views/';
	$view -> pagetitle = "確認画面";
	$view -> name = $_POST['name'];
	$view -> password = $_POST['password'];
	$view -> action = 'http://co-647.99sv-coco.com/kadai_3/kadai_7/sign/up_done';
	$view -> show('up_conf.php');
    }
    
    public function up_doneAction(){
	$sign = new Sign();
	$sign -> create_table();
	$id = $sign -> makeID();

	$sign -> register($id, $_POST['name'], $_POST['password']);

	$a = $sign -> getdata('members1');
	$view = new TemplateEngine();
	$view -> template_dir = '/views/';
	$view -> pagetitle = "登録完了";
	$view -> ref = $a;
	$view -> show('up_done.php');
    }

    public function inAction(){
	$sign = new Sign();

	$view = new TemplateEngine();
	$view -> template_dir = '/views/';
	$view -> pagetitle = "ログイン";
	$view -> action = 'http://co-647.99sv-coco.com/kadai_3/kadai_7/';
	$view -> members = $sign -> getdata('members1');
	$view -> show('in.php');
    }
}
