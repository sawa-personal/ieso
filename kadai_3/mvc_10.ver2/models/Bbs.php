<?php
class Bbs{

    private $pdo;
    private $tablename;

    public function __construct(){
        $this->pdo = new PDO('mysql:host=localhost;dbname=co_647_it_3919_com;charset=utf8', 'co-647.it.3919.c', 'jBi8Hbd');
	$this->tablename = 'bbs3';
    }

    public function createtable(){
	//$this->pdo->exec("DROP TABLE IF EXISTS {$this->tablename}");
	$this->pdo->query( "CREATE TABLE IF NOT EXISTS {$this->tablename}"
			  ."("
			  ."no INT auto_increment primary key," //投稿no
	    //."id INT," //掲示板id(id)
			  ."uid CHAR(8)," //投稿者id(uid)
			  ."name CHAR(255),"
			  ."comment VARCHAR(2048),"
			  ."object LONGBLOB," /*バイナリ*/
			  ."ext char(8)," /*拡張子*/
			  ."time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,"
			  ."delflag BOOL" /*削除フラグ*/
			  .");");
    }

    /*スレッド一覧を取得*/
    public function getThreadList(){
	//最新の10件を取得
    }

    /*スレッドの内容を取得*/
    public function getThread(){
	$stmt = $this -> pdo -> query("SELECT * FROM {$this->tablename}");
	return $stmt -> fetchAll();
    }

    /*スレッド作成*/
    public function makeThread(){

    }

    /*スレッド削除*/
    public function deleteThread(){

    }

    public function getExt($path){
	return pathinfo($path, PATHINFO_EXTENSION);
    }

    /*書込み*/
    public function writeRes($id, $name, $comment, $upfile){
	$object = file_get_contents($upfile['tmp_name']);
	$ext = $this -> getExt($upfile['name']);
	$stmt = $this -> pdo -> prepare("INSERT INTO {$this->tablename} (no,uid,name,object,ext,comment,delflag) VALUES ('', :uid, :name, :object, :ext, :comment, :delflag)");
	$stmt -> bindValue(':uid', $id, PDO::PARAM_STR);
	$stmt -> bindValue(':name', $name == "" ? "Anonymous" : $name, PDO::PARAM_STR);
	$stmt -> bindValue(':comment', $comment, PDO::PARAM_STR);
	$stmt -> bindValue(':object', $object, PDO::PARAM_STR);
	$stmt -> bindValue(':ext', $ext, PDO::PARAM_STR);
	$stmt -> bindValue(':delflag', false, PDO::PARAM_STR);
	$stmt -> execute();
    }	

    /*書込み削除*/
    public function deleteRes($id, $no){
	$stmt = $this -> pdo -> prepare("UPDATE {$this->tablename} SET delflag =:delflag WHERE no = '{$no}'");
	$stmt->bindValue(':delflag', true, PDO::PARAM_STR);
	$stmt->execute();
    }

    /*書込み編集*/
    public function editRes($id, $no, $comment){
	$stmt = $this -> pdo -> prepare("UPDATE {$this->tablename} SET comment =:comment WHERE no = '{$no}'");
	$stmt->bindValue(':comment', $comment, PDO::PARAM_STR);
	$stmt->execute();
    }

    public function showImage($id, $ext){	
	switch ($ext) {
	    case 'jpg': $mime = 'image/jpeg'; break;
	    case 'gif': $mime = 'image/gif'; break;
	    case 'png': $mime = 'image/png'; break;
	    case 'mp4': $mime = 'video/mp4'; break;
	    case 'ogv': $mime = 'video/ogg'; break;
	}

	header("Content-type: $mime");
	$re = $this->getThread();
	echo $re[$id-1]['object'];
    }

}
