<?php

class Dispatcher{

    private $sysRoot;
    private $loginFlag;

    public function setSystemRoot($path){
	$this->sysRoot = rtrim($path, '/');//末尾のスラッシュ削除
    }

    public function dispatch(){
	// パラメーター取得
	$param = ereg_replace(dirname($_SERVER["SCRIPT_NAME"]).'/', '', $_SERVER['REQUEST_URI']);
	//末尾の / を削除
	$param = ereg_replace('/?$', '', $param);
	$params = array();
	if ('' != $param) {
	    // パラメーターを / で分割
	    $params = explode('/', $param);
	}
	$get = ereg_replace('/?$', '', $_GET['a']);
	//echo $get;

	// １番目のパラメーターをコントローラーとして取得
	$controller = "bbs";
	if (0 < count($params)) {
	    $controller = $params[0];  //もともとは$param[0]
	}
	// １番目のパラメーターをもとにコントローラークラスインスタンス取得
        $controllerInstance = $this->getControllerInstance($controller);
        if (null == $controllerInstance) {
            $this->print404();
        }

	// 2番目のパラメーターをアクションとして取得
	$action= 'read';
	if (1 < count($params)) {
	    $action= $params[1];
	}

	// アクションメソッドの存在確認
        if (false == method_exists($controllerInstance, $action . 'Action')) {
            $this->print404();
        }

	$controllerInstance->setControllerAction($controller, $action); //コントローラ初期設定
	$controllerInstance->run(); //処理実行
    }


    // コントローラークラスのインスタンスを取得
    private function getControllerInstance($controller){
        // 一文字目のみ大文字に変換＋"Controller"
        $className = ucfirst(strtolower($controller)) . 'Controller';
        // コントローラーファイル名
        $controllerFileName = sprintf('%s/controllers/%s.php', $this->sysRoot, $className);
        // ファイル存在チェック
        if (false == file_exists($controllerFileName)) {
            return null;
        }
        // クラスファイルを読込
        require_once $controllerFileName;
        // クラスが定義されているかチェック
        if (false == class_exists($className)) {
            return null;
        }
        // クラスインスタンス生成
        $controllerInstarnce = new $className();

        return $controllerInstarnce;
    }

    private function print404(){
	header("HTTP/1.0 404 Not Found");
	include('404.html');
	exit;
    }    
}
?>
