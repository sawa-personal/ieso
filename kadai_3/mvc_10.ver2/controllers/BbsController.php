<?php
class BbsController extends XXXController{

    function preAction(){ //共通処理
	session_start();
	$doesLogin = isset($_SESSION['username']);

	$param = ereg_replace(dirname($_SERVER["SCRIPT_NAME"]).'/', '', $_SERVER['REQUEST_URI']); // パラメーター取得
	$param = ereg_replace('/?$', '', $param);
	$params = array();
	if ('' != $param) {
	    $params = explode('/', $param);
	}
	$currentPath = ROOT_URL.'/'.$params[0].'/'.$params[1];
	$currentPath = rtrim($currentPath, '/'); //末尾のスラッシュ削除
	if(!$doesLogin && $currentPath != ROOT_URL && $currentPath != ROOT_URL.'/bbs/image'){
	    header('Location: '.ROOT_URL);
	    exit;
	}

	$this -> view -> doesLogin = $doesLogin;
	$this -> view -> userName = $doesLogin ? $_SESSION['username'] : 'Guest';
	$this -> view -> userID = $_SESSION['id'];
    }

    public function readAction(){
	$bbs = new Bbs();
	$bbs -> createTable();
	$this -> view -> pagetitle = 'トップぺージ';
	$this -> view -> action = ROOT_URL.'/bbs/post_done';
	$this -> view -> thread = $bbs -> getThread();
    }

    //スレ立て
    public function createAction(){
	$this -> view -> pagetitle = 'スレッド作成フォーム';
	$this -> view -> action = ROOT_URL.'/bbs/create_conf';
    }

    public function create_confAction(){
	$bbs = new Bbs();
	$bbs -> makeThread();

	$this -> view -> pagetitle = 'スレッド作成フォーム';
	$this -> view -> action = ROOT_URL.'/bbs/create_conf';

    }

    public function create_doneAction(){
	
    }

    //書込み
    public function post_doneAction(){
	$bbs = new Bbs();
	$bbs -> writeRes($_SESSION['id'],$_POST['name'],$_POST['comment'],$_FILES['upfile']);
	$this -> view -> pagetitle = '書込み内容確認';
	$this -> view -> action = ROOT_URL.'/bbs/post_done';
    }

    public function replyAction(){
	$this -> $view -> pagetitle = '$Bbs -> threadtitle()';
    }

    public function imageAction(){
	$bbs = new Bbs();
	$bbs -> showImage($_GET['id'],$_GET['type']);
    }

    //編集
    public function edit_confAction(){
	$this -> view -> action = ROOT_URL.'/bbs/edit_done';
    }

    public function edit_doneAction(){
	$bbs = new Bbs();
	$bbs -> editRes('スレッドID', $_POST['no'], $_POST['comment']);
    }

    //削除
    public function delete_confAction(){}

    public function delete_doneAction(){
	$bbs = new Bbs();
	$bbs -> deleteRes('スレッドID', $_POST['no']);
    }
}
