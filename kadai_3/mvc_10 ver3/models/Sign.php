<?php
class Sign{
    private $pdo;

    public function __construct(){
        $this->pdo = new PDO('mysql:host=localhost;dbname=co_647_it_3919_com;charset=utf8', 'co-647.it.3919.c', 'jBi8Hbd');
    }

    public function makeID(){
	return substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, 8);
    }

    public function makeUnique(){
	return '-'.substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, 7);
    }

    public function login($id, $password){
	session_start();
	$stmt = $this->pdo->query("SELECT * FROM members2 WHERE id='{$id}'");
	$re = $stmt -> fetch(PDO::FETCH_ASSOC);
	if($re){
	    if($re['id'][0] == '-'){
		echo 'メール認証が終わっていません．';
	    } else if($re['password'] != $password){
		echo 'Passwordが間違っています．';
	    }
	    $_SESSION['username'] = $re['name'];
	    $_SESSION['id'] = $re['id'];
	    echo 'ログイン成功';
	} else {
	    echo 'IDが存在しません．';
	}
	header('Location: '.$_GET['url']);
    }

    public function logout(){
	session_start();
	unset($_SESSION['username']);
	unset($_SESSION['id']);
	session_destroy();

	header('Location: '.ROOT_URL);
    }
    
    public function create_table(){
	$this->pdo->query( "CREATE TABLE IF NOT EXISTS members2"
			  ."("
			  ."id CHAR(8) primary key,"
			  ."name VARCHAR(64),"
			  ."email VARCHAR(512),"
			  ."password VARCHAR(64)"
			  .");");
    }

    public function tempRegister($unique, $name, $email, $password){
	$stmt = $this -> pdo -> prepare("INSERT INTO members2 (id,name,email,password) VALUES (:id, :name, :email, :password)");
	$stmt -> bindValue(':id', $unique, PDO::PARAM_STR);
	$stmt -> bindValue(':name', $name, PDO::PARAM_STR);
	$stmt -> bindValue(':email', $email, PDO::PARAM_STR);
	$stmt -> bindValue(':password', $password, PDO::PARAM_STR);
	$stmt -> execute();
    }

    public function register($unique){
	$id = $this->makeID();
	$stmt = $this -> pdo -> prepare("UPDATE members2 SET id =:id WHERE id = '{$unique}'");
	$stmt->bindValue(':id', $id, PDO::PARAM_STR);
	$stmt->execute();

	$stmt = $this->pdo->query("SELECT * FROM members2 WHERE id='{$id}'");
	$re = $stmt -> fetch(PDO::FETCH_ASSOC);
	echo $re['id']." ".$re['password']."******";
	return array($re['id'], $re['password']);
    }
    
    public function sendMail($email, $unique){
	$subject = "会員登録";
	$body = ROOT_URL."/sign/register/?uid=$unique";
	mb_language("japanese");
	mb_internal_encoding("UTF-8");	
	if (mb_send_mail($email, $subject, $body, "")) {
	    echo "メールが送信されました．";
	} else {
	    echo "メールの送信に失敗しました．";
	}
    }

    public function getdata(){
	$stmt = $this -> pdo -> query("SELECT * FROM members2");
	return $stmt -> fetchAll();
    }
}
