<?php
class BbsController extends XXXController{

    function preAction(){ //共通処理
	/*
	   session_start();
	   $doesLogin = isset($_SESSION['name']);
	   $this -> view -> doesLogin = $doesLogin;
	   $this -> view -> userName = $doesLogin ? $_SESSION['name'] : 'Guest';
	   $this -> view -> userID = $_SESSION['id'];	
	 */
	$doesLogin = isset($_COOKIE['username']);
	$this -> view -> doesLogin = $doesLogin;
	$this -> view -> userName = $doesLogin ? $_COOKIE['username'] : 'Guest';
	$this -> view -> userID = $_COOKIE['id'];	
    }

    public function readAction(){
	$sign = new Sign();
	if($_POST['f_login'] == 'true'){
	    $sign -> login($_POST['loginID'],$_POST['loginPassword']);
	}
	//$bbs -> create_table();
	$bbs = new Bbs();	
	$this -> view -> pagetitle = "トップぺージ";
	$this -> view -> action = ROOT_URL.'/bbs/post_conf';
	$this -> view -> thread = $bbs -> getThread();
    }

    public function confAction(){
    }


    public function imageAction(){
	$bbs = new Bbs();
	$bbs -> showImage($_GET['id'],$_GET['type']);
    }

    public function post_confAction(){
	$bbs = new Bbs();
	$bbs -> writeRes($_POST['id'],$_POST['name'],$_POST['comment'],$_FILES['upfile']['name'],$_FILES['upfile']['tmp_name']);

	$this -> view -> pagetitle = '書込み内容確認';
    }

    public function replyAction(){
	$view = new TemplateEngine();
	$view -> template_dir = '/views/';
	$view -> pagetitle = '$Bbs -> threadtitle()';
	$view -> show('thread.php');
    }
}
