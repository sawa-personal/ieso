<?php
class SignController extends XXXController {

    public function inAction(){
	$sign = new Sign();
	//$sign -> create_table();
	$this -> view -> pagetitle = "ログイン";
	$this -> view -> action = ROOT_URL;
	$this -> view -> members = $sign -> getdata('members2');
    }

    public function outAction(){
	$sign = new Sign();
	$sign -> logout();
    }

    public function upAction(){
	$this -> view -> pagetitle = "会員登録";
    }

    public function up_confAction(){
	if($_POST['password'] == ''){
	    echo 'パスワードを入力してください．';
	    exit();
	}
	$this -> view -> pagetitle = "確認画面";
	$this -> view -> name = $_POST['name'];
	$this -> view -> email = $_POST['email'];
	$this -> view -> password = $_POST['password'];
	$this -> view -> action = ROOT_URL.'/sign/up_sent';
    }
    
    public function up_sentAction(){
	$sign = new Sign();

	$unique = $sign -> makeUnique();

	$sign -> tempRegister($unique, $_POST['name'], $_POST['email'], $_POST['password']);
	$sign -> sendMail($_POST['email'], $unique);

	$a = $sign -> getdata();
	$this -> view -> pagetitle = "メール送信完了";
	$this -> view -> ref = $a;
    }

    public function registerAction(){
	$sign = new Sign();
	list($id, $password) = $sign -> register($_GET['uid']);

	$this -> view -> pagetitle = "登録完了";
	$this -> view -> id = $id;
	$this -> view -> id = $password;
    }
}
