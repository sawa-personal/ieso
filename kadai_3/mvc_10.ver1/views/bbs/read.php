<?php include 'header.php'; ?>
<dl>
    <?php foreach($thread as $res){ ?>
	<dt><?=$res['no']?>:<?=$res['name']?> <b><?=$res['time']?></b> ID:<?=$res['id']?></dt>
	<dd>
	    <?php if($res['ext'] == 'png' || $res['ext'] == 'jpg' || $res['ext'] == 'gif'){?>
		<img src="<?=ROOT_URL?>/bbs/image/?id=<?=$res['no']?>&type=<?=$res['ext']?>" />
	    <?php } ?>
	    <?php if($res['ext'] == 'ogv' || $res['ext'] == 'ogg' || $res['ext'] == 'mp4'){?>
		<video controls>
		    <source src="<?=ROOT_URL?>/bbs/image/?id=<?=$res['no']?>&type=<?=$res['ext']?>">
		</video>
	    <?php } ?>
	    <p><?=$res['comment']?></p>
	    <p><?=$res['ext']?></p>
	</dd>
    <?php } ?>
</dl>

<?php if($doesLogin) { ?>
<h3>コメント</h3>
<form action="<?=$action?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="name" value="<?=$userName?>">
    <table>
	<tr><th>コメント</th><td><textarea cols="40" rows="8" name="comment"></textarea></td></tr>
	<tr><th>画像/動画</th><td><input type="file" name="upfile"></td></tr>
    </table>
    <input type="submit" value="投稿" />
</form>
<?php } ?>

<?php include 'footer.php'; ?>
