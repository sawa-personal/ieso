<?php

class Bbs{

    private $pdo;

    public function __construct(){
        $this->pdo = new PDO('mysql:host=localhost;dbname=co_647_it_3919_com;charset=utf8', 'co-647.it.3919.c', 'jBi8Hbd');
    }


    public function create_table(){
	$this->pdo->query( "CREATE TABLE IF NOT EXISTS bbs2"
			  ."("
			  ."no INT auto_increment primary key,"
			  ."id CHAR(8),"
			  ."name CHAR(255),"
			  ."comment VARCHAR(2048),"
			  ."object BLOB(2048),"
			  ."time TIMESTAMP DEFAULT CURRENT_TIMESTAMP"
			  .");");
    }

    /*スレッド一覧を取得*/
    public function getThreadList(){
	//最新の10件を取得
    }

    /*スレッドの内容を取得*/
    public function getThread(){
	$stmt = $this -> pdo -> query("SELECT * FROM bbs1");
	return $stmt -> fetchAll();
    }

    /*スレッド作成*/
    public function makeThread(){

    }

    /*スレッド削除*/
    public function deleteThread(){

    }

    /*書込み*/
    public function writeRes($id,$name,$comment){
	$stmt = $this -> pdo -> prepare("INSERT INTO bbs1 (no,id,name,comment) VALUES ('', :id, :name, :comment)");
	$stmt -> bindValue(':id', $id, PDO::PARAM_STR);
	$stmt -> bindValue(':name', $name == "" ? "Anonymous" : $name, PDO::PARAM_STR);
	$stmt -> bindValue(':comment', $comment, PDO::PARAM_STR);
	$stmt -> execute();
    }	

    /*書込み削除*/
    public function deleteRes(){

    }

    /*書込み編集*/
    public function editRes(){

    }
}
