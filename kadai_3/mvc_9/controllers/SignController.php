<?php
class SignController{

    public function inAction(){
	$sign = new Sign();
	$sign -> create_table();

	$view = new TemplateEngine();
	$view -> template_dir = '/views/';
	$view -> pagetitle = "ログイン";
	$view -> action = 'http://co-647.99sv-coco.com/kadai_3/kadai_9/';
	$view -> members = $sign -> getdata('members2');
	$view -> show('in.php');
    }

    public function upAction(){
	$view = new TemplateEngine();
	$view -> template_dir = '/views/';
	$view -> pagetitle = "会員登録";
	$view -> action = 'http://co-647.99sv-coco.com/kadai_3/kadai_9/sign/up_conf';
	$view -> show('up.php');
    }

    public function up_confAction(){
	$view = new TemplateEngine();
	if($_POST['password'] == ''){
	    echo 'パスワードを入力してください．';
	    exit();
	}
	$view -> template_dir = '/views/';
	$view -> pagetitle = "確認画面";
	$view -> name = $_POST['name'];
	$view -> email = $_POST['email'];
	$view -> password = $_POST['password'];
	$view -> action = 'http://co-647.99sv-coco.com/kadai_3/kadai_9/sign/up_sent';
	$view -> show('up_conf.php');
    }
    
    public function up_sentAction(){
	$sign = new Sign();

	$unique = $sign -> makeUnique();

	$sign -> tempRegister($unique, $_POST['name'], $_POST['email'], $_POST['password']);
	$sign -> sendMail($_POST['email'], $unique);

	$a = $sign -> getdata();
	$view = new TemplateEngine();
	$view -> template_dir = '/views/';
	$view -> pagetitle = "メール送信完了";
	$view -> ref = $a;
	$view -> show('up_sent.php');
    }

    public function registerAction(){
	$sign = new Sign();
	list($id, $password) = $sign -> register($_GET['uid']);

	$view = new TemplateEngine();
	$view -> template_dir = '/views/';
	$view -> pagetitle = "登録完了";
	$view -> id = $id;
	$view -> password = $password;
	$view -> show('register.php');
    }
}
