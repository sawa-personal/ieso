<?php

class Ref{
    private $pdo;

    public function __construct(){
        $this->pdo = new PDO('mysql:host=localhost;dbname=co_647_it_3919_com;charset=utf8', 'co-647.it.3919.c', 'jBi8Hbd');
    }

    public function create_table(){
	$this->pdo->query( "CREATE TABLE IF NOT EXISTS bibliography"
			  ."("
			  ."no INT auto_increment primary key,"
			  ."url VARCHAR(2048),"
			  ."title VARCHAR(2048)"
			  .");");
    }

    public function insert($url, $title){
	$stmt = $this -> pdo -> prepare("INSERT INTO bibliography (url,title) VALUES (:url, :title)");
	$stmt -> bindValue(':url', $url, PDO::PARAM_STR);
	$stmt -> bindValue(':title', $title, PDO::PARAM_STR);
	$stmt -> execute();
    }

    public function getdata($tablename){
	$stmt = $this -> pdo -> query("SELECT * FROM $tablename");
	return $stmt -> fetchAll();
    }
}
