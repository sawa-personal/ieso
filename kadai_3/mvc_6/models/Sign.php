<?php

class Sign{
    private $pdo;

    public function __construct(){
        $this->pdo = new PDO('mysql:host=localhost;dbname=co_647_it_3919_com;charset=utf8', 'co-647.it.3919.c', 'jBi8Hbd');
    }

    public function makeID(){
	return substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, 8);
    }

    public function create_table(){
	$this->pdo->query( "CREATE TABLE IF NOT EXISTS members1"
			  ."("
			  ."id CHAR(8) primary key,"
			  ."name VARCHAR(64),"
			  ."password VARCHAR(64)"
			  .");");
    }

    public function register($id, $name, $password){
	$stmt = $this -> pdo -> prepare("INSERT INTO members1 (id,name,password) VALUES (:id, :name, :password)");
	$stmt -> bindValue(':id', $id, PDO::PARAM_STR);
	$stmt -> bindValue(':name', $name, PDO::PARAM_STR);
	$stmt -> bindValue(':password', $password, PDO::PARAM_STR);
	$stmt -> execute();
    }

    public function getdata($tablename){
	$stmt = $this -> pdo -> query("SELECT * FROM $tablename");
	return $stmt -> fetchAll();
    }
}
