<?php
class BbsController{

    public function readAction(){
	$bbs = new Bbs();
	$bbs -> create_table();

	$view = new TemplateEngine();
	$view -> template_dir = '/views/';
	$view -> pagetitle = "トップぺージ";
	$view -> action = 'http://co-647.99sv-coco.com/kadai_3/kadai_6/bbs/post_conf';
	$view -> thread = $bbs -> getThread();
	$view -> show('read.php');
    }

    public function confAction(){
	echo 'hoge';
    }

    public function post_confAction(){
	$bbs = new Bbs();
	$bbs -> writeRes('1a2b3c4d','hogehoge',$_POST['comment']);

	$view = new TemplateEngine();
	$view -> template_dir = '/views/';
	$view -> pagetitle = '書込み内容確認';
	$view -> show('post_conf.php');
    }

    public function replyAction(){
	$view = new TemplateEngine();
	$view -> template_dir = '/views/';
	$view -> pagetitle = '$Bbs -> threadtitle()';
	$view -> show('thread.php');
    }
}
