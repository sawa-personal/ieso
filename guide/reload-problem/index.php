﻿<html>
    <head>
	<title><?php echo ($title = "フォームのリロードに関する問題"); ?></title>
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
    </head>
    <body>
	<h1><?php echo $title; ?></h1>
	<h2>リダイレクトによる実装</h2>
	<h3>概要</h3>
	<h3>実装例</h3>
	<?php $ex1 = 'http://co-647.99sv-coco.com/guide/reload-problem/redirect.php'; ?>
	<a href="<?php echo $ex1; ?>"> <?php echo $ex1; ?> </a>

	<h2>fetch()関数</h2>
	<h3>概要</h3>
	<h3>実装例</h3>
	<?php $ex1 = 'http://co-647.99sv-coco.com/guide/reload-problem/referer.php'; ?>
	<a href="<?php echo $ex1; ?>"> <?php echo $ex1; ?> </a>
	<h3>解説</h3>
	ページ遷移は[このページ]->[掲示板]なので問題なく表示されます．
	しかし更新ボタンを押すと，ページ遷移は[掲示板]->[掲示板]となりますから，
	これを不正な経路によるアクセスとみなしてエラーページにリダイレクトされるという
	ことでしょうか(詳しくないので憶測)．

	<h2>セッションによる実装</h2>
	<h3>概要</h3>
	<p>	上記の方法はいずれも，別のページを用意する必要がありましたが，1ページ完結に
	    こだわる場合は，セッションによる方法を用いるようです．
	    実装方法は割愛します．参考資料はページ末尾にまとめてあります．
	</p>
	<h3>実装例</h3>
	<?php $ex1 = 'http://co-647.99sv-coco.com/guide/reload-problem/referer.php'; ?>
	<a href="<?php echo $ex1; ?>"> <?php echo $ex1; ?> </a>
	<h2>参考資料</h2>
	<ul>
	    <li></li>
	    <li></li>
	</ul>
    </body>
</html>
