﻿<html>
    <head>
	<title><?php echo ($title = "不正なアクセスです．"); ?></title>
	<link rel="stylesheet" type="text/css" href="./style.css" />
    </head>
    <body>
	<h1><?php echo $title; ?></h1>
	<div class="box">
            不正なアクセスです．
	    お手数ですが，最初の画面に戻ってやり直してください．
	    <a href="referer.php">もどる</a>
	</div>
	<p>	このようなページに飛ばされます．
	    個人情報などの重要情報を入力させる画面ではよく見かけますが，
	    普通の掲示板ではあまり見かけない気がします．
	</p>
    </body>
</html>
