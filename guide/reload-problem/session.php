﻿<html>
    <head>
	<title><?php echo ($title = "セッションによる実装"); ?></title>
	<link rel="stylesheet" type="text/css" href="./style.css" />
    </head>
    <body>

	<?php  
	session_start();
	if (isset($_SESSION["counter"])) {
	    $_SESSION["counter"]++;
	    print($_SESSION["counter"]."回目の読み込みです。");
	} else {  
	    $_SESSION["counter"] = 0;  
	    print("はじめての読み込みです。");  
	}
	?>

	<?php 
	$name = $_POST['name'];
	$comment = $_POST['comment'];
	$f_com =  $_POST['f_com'];
	?>

	<form action="./session.php" method="post">
	    <?php $_SESSION['execute'] = false; ?>
	    <table>
		<tr><th>名前</th><td><input type="text" name="name"></td></tr>
		<tr><th>コメント</th><td><input type="text" name="comment"></td></tr>
		<input type="hidden" name="f_com" value="true">
		<tr><td colspan="2"><input type="submit" value="投稿" /></td></tr>
	    </table>
	    <?php $_SESSION['execute'] = true; ?>
	</form>

	<?php
	$file = 'data.txt';

	//2-2 書き込み処理
	$fp = fopen($file, 'r');
	for($number = 0; fgets($fp); $number++);
	fclose($fp);

	$buf = file_get_contents($file);
	if($number > 0){
	    $buf .= "\n";
	}
	$buf .= $number + 1;
	$buf .= "<>";
	$buf .= $name;
	$buf .= "<>";  $buf .= $comment;
	$buf .= "<>";
	$buf .= date('Y年m月d日H時i分s秒');
	if($f_com == "true"){
	    file_put_contents($file, $buf);
	}
	//2-3表示処理
	$buf = file_get_contents($file);
	$ary = explode("\n", $buf);
	$len = 5;
	$table[1000]; //No,Name,Comment,Date
	for($i = 0; $i < $number+1; $i++){
	    $table[$i] = explode("<>", $ary[$i]);;
	}
	?>

	<table>
	    <?php
	    for($i = 0; $i < $number+1; $i++){
		echo "<tr>";
		echo "<td>{$table[$i][0]}</td>";
		echo "<td>{$table[$i][1]}</td>";
		echo "<td>{$table[$i][2]}</td>";
		echo "<td>{$table[$i][3]}</td>";
		echo "<tr>\n";
	    }
	    ?>
	</table>
    </body>
</html>
<?php exit(); ?>

