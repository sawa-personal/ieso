<html>
<?php $title = "課題2参考ページ"; ?>
  <head>
      <title><?php echo $title; ?></title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" type="text/css" href="/css/style.css">
  </head>
  <body>
    <h1><?php echo $title; ?></h1>
    <p>このページでは，プログラミングをする上で必須の「繰り返し処理」と「条件分岐」について解説いたします．未経験者の方は是非，このページのプログラムを実際に動かしてみてください．</p>
    <p>このページは課題2の直接の解説ではありません(しかし必須の知識です)．また今後内容を大幅に変更する可能性がありますので，ご了承ください．</p>
    <h2>(1)for文を用いた繰り返し処理</h2>
    <p>次のプログラムを実行せよ：</p>
    <pre><?php
	 echo htmlentities(file_get_contents('for.txt'));
	 ?></pre>
    <h3>実行例</h3>
    <a href="http://co-647.99sv-coco.com/guide/kadai_2/for.php">http://co-647.99sv-coco.com/guide/kadai_2/for.php</a>
    <h3>解説</h3>
    <p>同じ処理を何度も繰り返す場合，<strong>for文</strong>を使います．</p>
    <p>例えば上のプログラムは，"ahaha"と5回表示させるプログラムになっています．</p>
    <p>一般に，同じ処理をN回繰り返す場合は，次のように書くのが「定石」となっていますので，ぜひ覚えておきましょう：</p>
    <pre><?php
	 echo file_get_contents('for_temprate.txt');
	 ?></pre>
    <h3>もう少し詳細な説明</h3>
    <p>準備中です．プログラムだけ貼っておきますので，動作させてみてください．</p>
    <p>次のプログラムを実行せよ：</p>
    <pre><?php
	 echo htmlentities(file_get_contents('for_count.txt'));
	 ?></pre>
    <h3>実行例</h3>
    <a href="http://co-647.99sv-coco.com/guide/kadai_2/for_count.php">http://co-647.99sv-coco.com/guide/kadai_2/for_count.php</a>

    <p>次のプログラムを実行せよ：</p>
    <pre><?php
	 echo htmlentities(file_get_contents('for_array.txt'));
	 ?></pre>
    <h3>実行例</h3>
    <a href="http://co-647.99sv-coco.com/guide/kadai_2/for_array.php">http://co-647.99sv-coco.com/guide/kadai_2/for_array.php</a>

    <!--
	 for(ループ開始時に一度だけ実行される処理 ;ループ継続条件; 1ループした時の処理){
	 ループさせる処理
	 }
	 
	 <strong>0から始まる連続した番号が割り振られた箱</strong>だと思ってください．
	 コインロッカーをイメージするといいかもしれまん．

	 なぜ配列の特性上，番号は0から始める
	 かまいませんが，
	 <h2>例題2-2</h2>

	 <h3>参考資料</h3>
	 <ul class="rf">
	 <li><a href="http://php.net/manual/ja/function.file-get-contents.php">PHP: file_get_contents - Manual</a></li>
	 </ul>
	 
	 <h2>1-4</h2>
	 <h3>参考資料</h3>
	 <ul class="rf">
	 <li><a href="http://qiita.com/glaytomohiko/items/e02e4a667e8589c0a07d">【PHP】【HTML】関数とform送信を利用したプログラム - Qiita</a></li>
	 </ul>
         
	 <h2>1-6</h2>
	 <h3>参考資料</h3>
	 <ul class="rf">
	 <li><a href="http://www.php-labo.net/tutorial/php/file.html#add">ファイル入出力 | PHP Labo #ファイルへの追加書き込み</a></li>
	 </ul>
	 
	 <h2>1-7</h2>
	 <h3>参考資料</h3>
	 <ul class="rf">
	 <li><a href="http://php.net/manual/ja/language.types.array.php">PHP: 配列 - Manual</a></li>
	 <li><a href="http://doremi.s206.xrea.com/php/tips/array.html">PHPまとめ - 配列操作</a></li>
	 </ul>
       -->
  </body>
</html>
