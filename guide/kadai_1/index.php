﻿<html>
<?php $title = "課題1 参考ページ"; ?>
  <head>
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
  </head>
  <body>
      <h1><?php echo $title; ?></h1>
      頻繁に使う関数の使い方を覚えましょうって感じだと思います．

      <h2>例題1</h2>
      <h3>目標</h3>
      <p>echo()関数を用いて文字列を表示する．</p>
      とりあえず次のように打ってみましょう．
      <?php
      echo "あはは\nいひひ\n\nうふふ";
      ?>

      数字や文字列など，何かしらの値を格納するための「箱」のようなものは
      <strong>変数</strong>と呼ばれます．phpでは先頭に$記号がつくものは変数として
      扱われます．
      そして，実際に箱の中に値を格納することを<strong>代入する</strong>といいます．

      <?php
      $x = "hogehoge";
      echo $x;
      ?>

      結合してくれます．
      $a = "1";
      $b = "2";
      echo $a + $b;

      $a = 1;
      $b = 2;
      echo $a + $b;

      <pre>
	  <?php
	  $x = 1;
	  echo $x;
	  $x = 2;
	  echo $x;
	  ?>
      </pre>

      <h2>例題2</h2>

      <h3>目標2</h3>
      <p>file_put_contents()関数を用いて，プログラムから外部ファイルに文字列を出力する．</p>
      2.txt

      <h3>例題3</h3>
      <p>file_get_contents()関数を用いて，外部ファイルからプログラムに文字列を入力する．</p>

    <p><a href="../sus/">スタートアップセッション 参考ページ</a>参照</p>

    <h2>例題4</h2>
    <h3>目標</h3>
    <p>POSTを用いてフォームから入力されたデータを．</p>

    <h2>例題5</h2>
    <h3>目標</h3>
    <p>フォームから受け取った</p>
    <h3>解説</h3>
    <p>新しい知識は全く必要ありません．1から4が出来るなら自動的にできます．よって割愛．</p>

    <h2>例題5</h2>
    <h3>解説</h3>
    <p>新しい知識は全く必要ありません．1から4が出来るなら自動的にできます．よって割愛．</p>

    <h2>例題6</h2>
    <h3>目標</h3>
    <p>加算演算子や連結演算子を用いて，文字列同士をくっつけることができる．</p>
    要素と要素に対して
    連結演算子．なんか仰々しい名前ですがつまるところ二つの文字列をくっつけるだけです．
    左右の値を入れ替えると意味が変わっちゃうんですね．

    以下にお絵かきします．

    <h2>例題6</h2>
    <h3>目標</h3>
    <p>加算演算子や連結演算子を用いて，文字列同士をくっつけることができる．</p>
    要素と要素に対して
    連結演算子．なんか仰々しい名前ですがつまるところ二つの文字列をくっつけるだけです．
    左右の値を入れ替えると意味が変わっちゃうんですね．

    <h2>例題7</h2>
    <h3>目標</h3>
    <p>配列の特性を理解する．explode()関数を用いて，文字列を配列に格納する．</p>
    <h4>配列とは</h4>
    変数の一種
    例を見た方が早いです．

    変数．
配列にはインデックスと呼ばれる，
配列には0か始まる連続した番号が割り振られます．
例を見た方が早いです．
    <h3>実装例</h3>
    <pre>aaa</pre>
    
    <ul class="rf">
      <li><a href="http://php.net/manual/ja/function.file-get-contents.php">PHP: file_get_contents - Manual</a></li>
    </ul>
    
    <h2>1-4</h2>
    <h3>参考資料</h3>
    <ul class="rf">
      <li><a href="http://qiita.com/glaytomohiko/items/e02e4a667e8589c0a07d">【PHP】【HTML】関数とform送信を利用したプログラム - Qiita</a></li>
    </ul>
<!--
    <h2>1-6</h2>
    <h3>参考資料</h3>
    <ul class="rf">
      <li><a href="http://www.php-labo.net/tutorial/php/file.html#add">ファイル入出力 | PHP Labo #ファイルへの追加書き込み</a></li>
    </ul>
    
    <h2>1-7</h2>
    <h3>参考資料</h3>
    <ul class="rf">
      <li><a href="http://php.net/manual/ja/language.types.array.php">PHP: 配列 - Manual</a></li>
      <li><a href="http://doremi.s206.xrea.com/php/tips/array.html">PHPまとめ - 配列操作</a></li>
    </ul>

    -->
  </body>
</html>
