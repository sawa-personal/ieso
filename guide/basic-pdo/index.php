<html>
    <head>
	<title><?php echo ($title = "PDO覚書き"); ?></title>
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
	<link rel="stylesheet" type="text/css" href="./style.css" />
    </head>
    <body>
	<h1><?php echo $title; ?></h1>
	<h2>表記</h2>
	<p>PDOでデータベース操作を行う際，変数は，用途に応じて慣習的な名前が付けられるようです．
	    以下に基本的な変数と，その変数に不随する関数を列挙します．変数名に表記揺れがあるものはカッコ書きにしてあります．</p>
	<div class="obj">
	    <div class="val">
		<h3>$pdo ($dbh, $db等とも)</h3>
		<p>接続した<strong>データベース</strong>を指し示すための変数．</p>
	    </div>
	    <div class="met">
		<h3>query()関数</h3>
		<u>データベース$pdoに対して</u>要求を送り，それに応じた結果セット(＝データベースから取得したデータ)を返す関数．
		要求するときは引数にMySQL文(文字列)を渡す．返り値は，後述の$stmtに格納する．
	    </div>
	</div>

	<div class="obj">
	    <div class="val">
		<h3>$stmt ($sth等とも)</h3>
		<p><strong>結果セット</strong>(＝データベースから取得したデータ)を格納するための変数．</p>
	    </div>
	    <div class="met">
		<h3>fetch()関数</h3>
		<p><u>結果セット$stmtの中身の行を，1行づつ，実行するごとに配列として取り出す関数．
		    引数には次のようなものがある：</p>
		<dl>
		    <dt>PDO::FETCH_ASSOC</dt>
		    <dd>実行する度に，上から1行づつデータを配列として取り出す．</dd>
		    <dt>PDO::FETCH_BOTH</dt>
		    <dd>結果セット</dd>
		</dl>
	    </div>
	    <div class="met">
		<h3>fetchAll()関数</h3>
		<p><u>結果セット$stmtに対して</u>全ての行が格納された二次元配列を返す関数．
		    引数には次のようなものがある：</p>
		<dl>
		    <dt>PDO::FETCH_ASSOC</dt>
		    <dd>実行する度に，上から1行づつデータを配列として取り出す．</dd>
		    <dt>PDO::FETCH_BOTH</dt>
		    <dd>結果セット</dd>
		</dl>
	    </div>
	</div>

	<h2>例</h2>
	<h2>参考文献</h2>
	<ol>
	    <li></li>
	    <li></li>
	    <li></li>
	</ol>
    </body>
</html>
