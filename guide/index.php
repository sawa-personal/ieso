﻿<html>
  <?php $title = "課題に関する参考情報"; ?>
  <head>
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
  </head>
  <body>
    <h1><?php echo $title; ?></h1>
    <p>ここは，チームメンバのプログラミングをサポートする目的で作成したページです．</p>
    <p>課題を進める上での参考や，備忘録などを書いていく予定です．</p>
    <ul>
      <li><a href="./sus/">スタートアップセッション</a></li>
      <li>課題1</li>
      <li>課題2</li>
      <li>課題3</li>
    </ul>
    <h2>備忘録</h2>
    <ul>
	<li><a href="./submit-flag/">フォームでボタンが押されたかどうかを判定する方法</a></li>
	<!--<li><a href="./reload-problem/">フォーム画面でリロードすると二重投稿される問題への対処</a></li>-->
    </ul>
  </body>
</html>
