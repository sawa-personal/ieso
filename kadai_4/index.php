<?php
// ルートURL
define('ROOT_URL', 'http://'.$_SERVER["SERVER_NAME"].'/kadai_4');
// システムのルートディレクトリパス
define('ROOT_PATH', realpath(dirname(__FILE__).'/../kadai_4_mvc'));
// ライブラリのディレクトリパス
define('LIB_PATH', realpath(dirname(__FILE__).'/../kadai_4_mvc/library'));
// 外部ライブラリのディレクトリパス
define('VENDORS_LIB_PATH', realpath(dirname(__FILE__).'/../vendors'));

// ライブラリとモデルのディレクトリをinclude_pathに追加
$includes = array(LIB_PATH, ROOT_PATH.'/models', VENDORS_LIB_PATH);
$incPath = implode(PATH_SEPARATOR, $includes);
set_include_path(get_include_path().PATH_SEPARATOR.$incPath);

require_once('Cache/Lite/Output.php');

$url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
$cache_id = $url; // IDのセット

$cache = new Cache_Lite_Output(
    array(
	'cacheDir' => ROOT_PATH.'/tmp/',
	'caching' => 'true', // キャッシュを有効に
	'lifeTime' => 10, // （生存時間：30分）
	'hashedDirectoryLevel' => 1, // ディレクトリ階層
    )
);

if(!$cache->start($url)){
    //Smartyのオートロード
    require_once 'smarty/Autoloader.php';
    Smarty_Autoloader::register();

    // クラスのオートロード・Smartyに合わせて修正
    function autoloader($className){
	include($className.".php");
    }
    spl_autoload_register("autoloader");


    require_once 'Dispatcher.php';

    $dispatcher = new Dispatcher();
    $dispatcher->setSystemRoot(ROOT_PATH);
    $dispatcher->dispatch();

    $cache->end();
}
